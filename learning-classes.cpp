#include <iostream>
using namespace std;

class Persona {
  char name[30];
  int age;

  public:
    void setData(void);
    void getData(void);
};

void Persona::setData(void){
  cout << "Ingrese el nombre:";
  cin >> name;
  cout << "Ingrese la edad:";
  cin >> age;
}

void Persona::getData(void){
  cout << "\nNombre:" << name;
  cout << "\nEdad:" << age;
}

int main(){
  Persona p;
  p.setData();
  p.getData();
  return 0;
}
