#include <iostream>
using namespace std;

int main(){
  float numA, numB,
    sumaAB, mediaAB;
  cout << "Introduzca dos números: ";
  cin >> numA;
  cin >> numB;

  sumaAB = numA + numB;
  mediaAB = sumaAB / 2;

  cout << "Suma = " << sumaAB << "!\n";
  cout << "Media = " << mediaAB << "!\n";

  return 0;
}
